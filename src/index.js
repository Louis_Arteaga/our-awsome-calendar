import "./main.scss";
import "bootstrap";

const moment = require('moment');
moment.locale('fr');

let now = moment(); // Date actuel
principal(now); // Appel de la fonction principal()






function principal(date) {

    let titleCal = document.querySelector('#date'); // Date de titre 
    titleCal.innerHTML = moment(date).format('MMMM YYYY'); // On lui donne le format mois + année

    date = moment(date).startOf('month').isoWeekday(1); // date = début du mois actuel
    let endMonth = moment(date).endOf('month').format('D'); //  endMonth = fin du mois actuel

    for (let i = 0; i < endMonth; i++) {  // La boucle pose les jours du début du mois actuel à la fin de celui ci.
        // date = moment(date); // isoWeekday(1), permet de mettre le 1er jour du mois au jour correspondant.
        posDay(moment(date), moment().format('d'));
    }
}

function posDay(date, day) { // Fonction permettant de positionner les jours dans les semaines.
    let days = document.querySelectorAll('.day-' + day);
    console.log('.day-' + day);

    for (const day of days) {
        day.textContent = date.format('D');
        // console.log(date.format('D'));
        day.style.color = '';

        if (date.month() !== now.month()) {
            day.innerHTML = '';
        }

        if (date.isSame(moment(), 'day')) {
            day.style.color = 'black';


        }
        date = moment(date).add(1, 'days');
    }


}

document.addEventListener('keydown', function (e) {
    if (e.key === 'ArrowRight') {
        reset();
        now = moment(now).add(1, 'month');
        principal(now);
    } else if (e.key === 'ArrowLeft') {
        reset();
        now = moment(now).subtract(1, 'month');
        principal(now);
    }
})

let leftMove = document.querySelector("#left");
leftMove.addEventListener('click', function (e) {
    now = moment(now).subtract(1, 'month');
    principal(now);
});

let rightMove = document.querySelector("#right");
rightMove.addEventListener('click', function (e) {
    now = moment(now).add(1, 'month');
    principal(now);
});

function reset() {
    let days = document.querySelectorAll('td');
    days.innerHTML = '';
}
